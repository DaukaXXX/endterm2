package main

import (
	greet "dauka2/greet/greetpb"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
)

type Server struct {
	greet.UnimplementedGreetServiceServer
}

func (s *Server) LongGreet(stream greet.GreetService_LongGreetServer) error {
	sum := 0
	count := 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			var r float64

			r = float64(float64(sum) / float64(count))
			println(r)
			println(sum)
			println(count)
			return stream.SendAndClose(&greet.LongGreetResponse{
				Result: r,
			})
		}
		if err != nil {
			log.Fatalf("Error from client stream: %v", err)
		}
		number := int(req.Greeting.GetNumber())
		sum += number
		count++
	}
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greet.RegisterGreetServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
