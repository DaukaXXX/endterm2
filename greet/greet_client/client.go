package main

import (
	"context"
	greet "dauka2/greet/greetpb"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"time"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := greet.NewGreetServiceClient(conn)
	doLongGreet(c)
}

func doLongGreet(c greet.GreetServiceClient) {

	requests := []*greet.LongGreetRequest{
		{
			Greeting: &greet.Greeting{
				Number: 1,
			},
		},
		{
			Greeting: &greet.Greeting{
				Number: 2,
			},
		},
		{
			Greeting: &greet.Greeting{
				Number: 3,
			},
		},
		{
			Greeting: &greet.Greeting{
				Number: 4,
			},
		},
	}

	ctx := context.Background()
	stream, err := c.LongGreet(ctx)
	if err != nil {
		log.Fatalf("error while calling LongGreet: %v", err)
	}

	for _, req := range requests {
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("error with getting response from server : %v", err)
	}
	fmt.Printf("Response: %v\n", res.GetResult())
}
